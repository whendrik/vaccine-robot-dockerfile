FROM rust:latest


RUN cd && git clone https://gitlab.com/whendrik/vaccine-robot
RUN cd /root/vaccine-robot && cargo build --release

RUN cd /root/vaccine-robot/target/release/
WORKDIR /root/vaccine-robot/target/release/

CMD ["./covid"]